package myArrayList.driver;

import java.util.InputMismatchException;
import myArrayList.MyArrayList;
import myArrayList.test.MyArrayListTest;
import myArrayList.util.FileProcessor;
import myArrayList.util.Result;

/**
 * @author prathamesh
 *
 */
public class Driver {

	/**
	 * @param args array of string arguments.
	 * @throws Exception when desired,
	 *  number of arguments not padded.
	 */
	public static void main(String[] args) throws Exception {
		String inputFileName = null;
		String outputFileName = null;
		if(args.length != 2) {
			throw new RuntimeException("Expected 2 args");
		}
		else{
			inputFileName = args[0];
			outputFileName = args[1];
		}
		FileProcessor fp = new FileProcessor(inputFileName);
		MyArrayList a = new MyArrayList();
		String number= null;
		while ((number = fp.readLine()) != null ) {
			int parseInt = Integer.parseInt(number);
			if(parseInt < 0 || parseInt > 10000){
				fp.closeScanner();
				throw new InputMismatchException("Invalid number expected: " + parseInt + 
						" Expected range 0 to 10000");
			}
			a.insertSorted(parseInt);
		}
		int sum = a.sum();
		String valueOfSum = String.valueOf(sum);
		Result result = new Result(outputFileName);
		MyArrayList myArray= new MyArrayList();
		MyArrayListTest test = new MyArrayListTest();
		test.testMe(myArray, result);
		result.writeToFile(valueOfSum);
		result.writeToStdout();
	}

}
