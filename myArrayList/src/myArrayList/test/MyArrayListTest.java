package myArrayList.test;

import myArrayList.MyArrayList;
import myArrayList.util.Result;

/**
 * @author prathamesh
 *
 */
public class MyArrayListTest {

	public void testMe(MyArrayList myArray, Result result) {
		myArray.clear();
		testComputingIndexInEmptyArray(myArray, result);
		
		myArray.clear();
		testDuplicatesSorted(myArray,result);
		
		myArray.clear();
		testSumSorted(myArray,result);

		myArray.clear();
		testRemoveValue(myArray,result);
		
		myArray.clear();
		testActualResizing(myArray, result);
		
		myArray.clear();
		testArrayComputationInsertAndRemove(myArray, result);
		
		myArray.clear();
		testInsertingZeros(myArray, result);
		
		myArray.clear();
		testComputingSumInEmptyArray(myArray, result);
		
		myArray.clear();
		testRemovingAllOccurences(myArray, result);
		
		myArray.clear();
		testComputingIndexOfDuplicate(myArray, result);
		
	}
	/**
	 * @param myArray of type MyArrayList .
	 * @param result of type Result, to store results of test.
	 */
	private void testRemovingAllOccurences(MyArrayList myArray, Result result){
		myArray.insertSorted(20);
		myArray.insertSorted(40);
		myArray.insertSorted(30);
		myArray.insertSorted(50);
		myArray.insertSorted(30);
		myArray.removeValue(30);
		int[] expectedArray = {20,40,50};
		// Code for printing methodName used from stack overflow 
		String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
		boolean isRemoveComputationSatisfied = assertEquals(expectedArray, myArray.toArray());
		if(isRemoveComputationSatisfied){
			result.storeNewResult( methodName + " PASSED");
		} else {
			result.storeNewResult( methodName + " FAILED");
		}
	}
	
	/**
	 * @param myArray of type MyArrayList.
	 * @param result of type Result, to store results of test.
	 */
	private void testComputingIndexOfDuplicate(MyArrayList myArray, Result result){
		myArray.insertSorted(20);
		myArray.insertSorted(40);
		myArray.insertSorted(30);
		myArray.insertSorted(50);
		myArray.insertSorted(30);
		boolean isIndexValueSatisfied = assertEquals(1, myArray.indexOf(30));
		// Code for printing methodName used from stack overflow
		String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
		if(isIndexValueSatisfied){
			result.storeNewResult( methodName + " PASSED");
		} else {
			result.storeNewResult( methodName + " FAILED");
		}
	}
	/**
	 * @param myArray of type MyArrayList.
	 * @param result of type Result, to store results of test.
	 */	
	private void testInsertingZeros(MyArrayList myArray, Result result){
		myArray.insertSorted(0);
		myArray.insertSorted(0);
		myArray.insertSorted(0);
		boolean isActualSizeSatisfied = assertEquals(3, myArray.Size());
		boolean isSumSatisfied =assertEquals(0, myArray.sum());
		// Code for printing methodName used from stack overflow
		String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
		if(isActualSizeSatisfied && isSumSatisfied){
			result.storeNewResult( methodName + " PASSED");
		} else {
			result.storeNewResult( methodName + " FAILED");
		}	
	}
	
	/**
	 * @param myArray of type MyArrayList.
	 * @param result of type Result, to store results of test.
	 */	
	private void testActualResizing(MyArrayList myArray, Result result) {
		int i;
		myArray.clear();
		for(i = 0; i < 50; i++){
			myArray.insertSorted(i);
		}
		boolean isActualSizeSatisfied = assertEquals(i, myArray.Size());
		// Code for printing methodName used from stack overflow
		String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
		if(isActualSizeSatisfied){
			result.storeNewResult( methodName + " PASSED");
		} else {
			result.storeNewResult( methodName + " FAILED");
		}	
		
	}
	/**
	 * @param myArray of type MyArrayList.
	 * @param result of type Result, to store results of test.
	 */
	private void testSumSorted(MyArrayList myArray, Result result){
		myArray.insertSorted(20);
		myArray.insertSorted(40);
		myArray.insertSorted(30);
		myArray.insertSorted(50);
		myArray.insertSorted(30);
		boolean isSumSatisfied =assertEquals(170, myArray.sum());
		// Code for printing methodName used from stack overflow
		String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
		if(isSumSatisfied) {
			result.storeNewResult( methodName + " PASSED");
		} else {
			result.storeNewResult( methodName + " FAILED");
		}
	}
	/**
	 * @param myArray of type MyArrayList.
	 * @param result of type Result, to store results of test.
	 */
	private void testRemoveValue(MyArrayList myArray, Result result){
		myArray.insertSorted(20);
		myArray.insertSorted(40);
		myArray.insertSorted(30);
		myArray.insertSorted(50);
		myArray.insertSorted(30);
		//Trying to remove value which is not present
		int[] expectedArray = {20,30,30,40,50};
		// Code for printing methodName used from stack overflow
		String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
		boolean isRemoveSatisfied = assertEquals(expectedArray, myArray.toArray());
		if (isRemoveSatisfied){
			result.storeNewResult( methodName + " PASSED");
		} else {
			result.storeNewResult( methodName + " FAILED");
		}
	}
	/**
	 * @param myArray of type MyArrayList.
	 * @param result of type Result, to store results of test.
	 */
	private void testDuplicatesSorted(MyArrayList myArray, Result result) {
		myArray.insertSorted(20);
		myArray.insertSorted(40);
		myArray.insertSorted(30);
		myArray.insertSorted(50);
		myArray.insertSorted(30);
		
		int[] expectedArray = {20,30,30,40,50};
		boolean isArraySatisifed = assertEquals(expectedArray, myArray.toArray());
		// Code for printing methodName used from stack overflow
		String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
		if(isArraySatisifed) {
			result.storeNewResult( methodName + " PASSED");
		} else {
			result.storeNewResult( methodName + " FAILED");
		}
	}
	/**
	 * @param myArray of type MyArrayList.
	 * @param result of type Result, to store results of test.
	 */
	private void testComputingSumInEmptyArray(MyArrayList myArray , Result result){
		boolean isSumSatisfied =assertEquals(0, myArray.sum());
		// Code for printing methodName used from stack overflow
		String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
		if(isSumSatisfied) {
			result.storeNewResult( methodName + " PASSED");
		} else {
			result.storeNewResult( methodName + " FAILED");
		}
	}
	/**
	 * @param myArray of type MyArrayList.
	 * @param result of type Result, to store results of test.
	 */
	private void testComputingIndexInEmptyArray(MyArrayList myArray , Result result) {		
		boolean isIndexSatisifed =assertEquals(-1, myArray.indexOf(2));
		String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
		if(isIndexSatisifed) {
			result.storeNewResult( methodName + " PASSED");
		} else {
			result.storeNewResult( methodName + " FAILED");
		}
	}
	/**
	 * @param myArray of type MyArrayList.
	 * @param result of type Result, to store results of test.
	 */
	private void testArrayComputationInsertAndRemove(MyArrayList myArray , Result result){
		myArray.insertSorted(3);
		myArray.insertSorted(4);
		myArray.insertSorted(50);
		myArray.removeValue(5);
		myArray.insertSorted(60);
		myArray.removeValue(3);
		myArray.insertSorted(70);
		
		int[] expectedArray = {4,50,60,70}; 
		boolean isArrayComputationSatisfied = assertEquals(expectedArray, myArray.toArray());
		String methodName = Thread.currentThread().getStackTrace()[1].getMethodName();
		if(isArrayComputationSatisfied) {
			result.storeNewResult( methodName + " PASSED");
		} else {
			result.storeNewResult( methodName + " FAILED");
		}
		
	}

	/**
	 * @param expectedVal 
	 * @param actualVal obtained from computing on myArray
	 * @return True if expectedVal == actualVals
	 */
	private boolean assertEquals(int expectedVal, int actualVal) {
		return expectedVal == actualVal;
	}

	/**
	 * @param expectedVal 
	 * @param actualVal obtained from computing on myArray
	 * @return True if expectedVal == actualVals
	 */	
	private boolean assertEquals(int[] expectedVal, int[] actualVal) {
		if(assertEquals(expectedVal.length, actualVal.length)) { //assert size first
			for(int i = 0 ; i < actualVal.length -1; i++) {
				if(!assertEquals(expectedVal[i], actualVal[i])) {
					return false;
				}
			}
			return true;
		}
		return false;
	}
}
