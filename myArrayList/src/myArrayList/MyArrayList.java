package myArrayList;

import java.util.Arrays;


public class MyArrayList {
	public static final int DEFAULT_SIZE = 50;
	private int [] myArray = null;
	private int size = 0;
	
	public MyArrayList(){
		myArray = new int[DEFAULT_SIZE];
	}
	
	public void clear() {
		this.size = 0;
		this.myArray = new int[DEFAULT_SIZE];
	}


	/** 
	 * 
	 * @param newValue insert in Array, resize array if required. 
	 */
	public void insertSorted(int newValue){
		if(size >= myArray.length ){
			int[] newMyArray = new int[myArray.length + Math.abs(myArray.length/2)];
			for(int i = 0; i < myArray.length; i++){
				newMyArray[i] = myArray[i];
			}
			myArray = newMyArray;
		}
		myArray[size] = newValue;
		Arrays.sort(myArray, 0, size+1);
		size++;
		
	}
	/**
	 * 
	 * @param value remove passed value from Array.
	 */
	public void removeValue(int value){
		int i = size -1;
		while(i >= 0) {
			if(myArray[i] == value){
				remove(i);
				size--;
				
			}
			i--;
		}
	}
	
	/**
	 * @param i, remove index of value to be removed
	 * and shift array   
	 */
	private void remove(int i){
		for(int n = i; n < size - 1; n++){
			myArray[n] = myArray[n+1];
			if (n == size - 1) {
				myArray[n+1] = 0; 
			}
		}
	}
	/**
	 * @param value, element whose index required
	 * @return index of the element passed
	 */
	public int indexOf(int value){
		for(int i = 0; i < size; i++){
			if (myArray[i] == value){
				return i;
			}
		}
		return -1;
	}
	
	/**
	 * @return sum of all the elements in array
	 */
	public int sum(){
		int sum = 0;
		for(int i = 0; i < size; i++){
			sum = sum + myArray[i];
		}
		return sum;
	}
	@Override
	public String toString() {
		StringBuffer buf = new StringBuffer();
		for(int i=0; i <=  size-1 ;i++) {
			if (i == (size - 1)) {
				buf.append(myArray[i]);
			} else {
				buf.append(myArray[i] + "\t");
			}
		}
		return buf.toString();
	}
	
	/**
	 * @return copied array
	 */
	public int[] toArray() {
		return Arrays.copyOf (myArray, size); //Make a copy and return so that no one can modify myArray
	}

	/**
	 * @return actual array length
	 */
	public int Size() {
		return size;
	}
	
}
	