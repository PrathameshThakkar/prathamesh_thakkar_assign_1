package myArrayList.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 * @author prathamesh
 *
 */
public class FileProcessor {
	
	private Scanner in = null;

	/**
	 * @param fileName file to be opened. 
	 * @throws Exception when unable to open file.
	 */
	public FileProcessor(String fileName) throws Exception {
		in = new Scanner(new File(fileName));
	}
	
	/**
	 * @return String, each line which is read
	 * @throws FileNotFoundException
	 */
	public String readLine() throws FileNotFoundException{
		try {
			
			if (in == null) {
				throw new RuntimeException("File aready closed");
			}
			String line;
			while(in.hasNext()){
				line = in.next();
				return line;
			}
			line = null;
			closeScanner();
			return line;
		} catch (Exception e) {
			closeScanner();
			throw e;
		}
	}
	
	/**
	 * To close scanner
	 */
	public void closeScanner() {
		if (in != null) {
			in.close();
			in = null;
		}
	}
}
