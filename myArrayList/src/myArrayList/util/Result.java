package myArrayList.util;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;

/**
 * @author prathamesh
 *
 */
public class Result implements FileDisplayInterface, StdoutDisplayInterface {
	private String fileName;
	private  ArrayList<String> testResults = new ArrayList<String>(); 
	
	
	@Override
	public void writeToFile(String s){
		PrintWriter out = null;
		try {
			out = new PrintWriter(this.fileName);
		} catch (FileNotFoundException e) {
			throw new RuntimeException(e);
		}
		int sumOut = Integer.parseInt(s);
		out.printf("The sum of all values in array list is: %d", sumOut); //This is from input.txt
		out.println();
		for (String res : testResults) {
			out.println(res);
		}
		out.close();
	}
	
	/**
	 * @param fileName initializing instance variable with
	 * given parameter
	 */
	public Result(String fileName){
		this.fileName = fileName;
	}
	
	@Override
	public void writeToStdout() {
		for (String s : testResults){
			System.out.println(s);
		}
	}
	
	/**
	 * @param res adding result to the ArrayList
	 */
	public void storeNewResult(String res){
		this.testResults.add(res);
	}
}
